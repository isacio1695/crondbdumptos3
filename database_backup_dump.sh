#!/bin/sh

# Este script hace un dump de la base de datos y lo envía a un bucket de AWS S3. Se coloca en el crontab para ser ejecutado de forma periódica
# Dependencias:
# MySQL
# AWS CLI
# Cuenta en servicio de almacenamiento S3 (AWS, DreamObjetcs) y llaves de acceso
# Archivo .my.cnf en la carpeta del usuario (root en estos casos)
# Reemplazar {nombre_db} por el nombre de la base de datos, {identificador} por algo que ayude a reconocer de qué aplicación
# es el backup y {nombre_bucket} por el nombre del bucket.

mysqldump {nombre_db} > {identificador}.$(date +%Y%M%d%H%m%S).sql

# Para utilizar con S3 compatible (Minio, etc.) colocar aws --endpoint-url https://url_de_servicio_s3
aws s3 cp . s3://{nombre_bucket} --recursive --exclude "*" --include "*.sql"
rm -rf *.sql
exit